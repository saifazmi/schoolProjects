#include<iostream>

using namespace std;

int main()
{
    char choice;
    unsigned int num;

    do
    {
        cout << "\nEnter a number: ";
        cin >> num;

        if(num%2 == 0)
            cout << "The number is Even";

        else
            cout << "The number is Odd";

        if (num == 1)
            cout<< " and Prime"<<endl;

        else
        {
            for(int i=2; i<=num/2; i++)
            if(num % i == 0)
            {
                cout << " not prime"<<endl;
                goto lb;
            }

            cout << " and Prime"<<endl;
        }
        lb :

        cout << "\n\nWant to continue? (y/n) : ";
        cin >> choice;
    }   while (choice == 'Y' || choice == 'y');
    return 0;
}
