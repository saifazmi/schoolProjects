#include <iostream>
#include <string>
   
      #include <ctime>
   
      using namespace std;
   
       
   
      int ask(const string& question){
   
      cout << question;
   
    int ans = 0;
   
      cin >> ans;
  
      return ans;
  
      }
  
       
        int main(){
  
      const int MAX_TIME_LIMIT = 10000; //10 seconds or 10 * 1000 milliseconds
  
      long start_time = clock();
  
       
  
      int ans = ask("What is 2 + 2 * 5 = ");
  
      int ans2 = ask("What is 6 mod 2 = ");
  
      int numOfCorrect = 0;
  
      long end_time = clock();
  
      if( end_time - start_time > MAX_TIME_LIMIT){
  
      cout << "Time limit exceeded\n";
  
      }
   if( ans == 12){ numOfCorrect++; }
  
      if( ans2 == 0){ numOfCorrect++; }
        
  
      cout << "You got " << numOfCorrect << " correct answer out of 2 question\n";
  
      cout << "Your percentage is " << 100*(numOfCorrect/2.0f) << endl;
  
       
  
      return 0;
  
      }