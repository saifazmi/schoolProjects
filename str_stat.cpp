#include <iostream>
#include <cctype>

using namespace std;

void str_stat(char str[], int &, int &, int &, int &, int &, int &);

int main()
{
    char string[250];
    int t_words, t_ch, up_vow, l_vow, up_conso, l_conso;

    cout << "Enter the string (max. 250 char) - \n" << endl;
    cin.getline(string, 250);

    str_stat(string,up_vow,l_vow,up_conso,l_conso,t_words,t_ch);

    cout << "\n\nUPPERCASE VOWELS:\t" << up_vow;
    cout << "\nLOWERCASE VOWELS:\t" << l_vow;
    cout << "\nUPPERCASE CONSONANTS:\t" << up_conso;
    cout << "\nLOWERCASE CONSONANTS:\t" << l_conso;
    cout << "\nTOTAL WORDS:\t" << t_words;
    cout << "\nTOTAL CHARACTERS:\t" << t_ch;

    return 0;
}

int words(char STR[])
{
    int count = 1;

    for(int i=0; STR[i] != '\0'; ++i)
    {
        if(STR[i] == ' ' || STR[i] == '\n')
        {
            ++count;

            while(STR[i] == ' ')
                ++i;
        }
    }

    return count;
}

int characters(char STR[])
{
    int count = 1;

    for(int i=0; STR[i] == '\0'; ++i)
    {
        if(isalnum(str[i]))
            ++count;
        else if
    }

    return count;
}

void str_stat(char str[], int &up_vow, int &l_vow, int &up_conso, int &l_conso, int &t_words, int &t_ch)
{
    char vow[]= {"aeiouAEIOU"};

    t_words = words(str);
    t_ch = characters(str);

    for(int i=0; str[i] != '\0'; ++i)
        for(int j=0; vow[j] == str[i]; ++j)
        {
            if(str[i] == vow[j])
            {
                if(isupper(str[i]))
                {
                    ++up_vow;

                    while(str[i] == ' ')
                    ++i;
                }

                else
                {
                    ++l_vow;

                    while(str[i] == ' ')
                    ++i;
                }
            }

            else
            if(isupper(str[i]))
            {
                ++up_conso;

                while(str[i] == ' ')
                    ++i;
            }

            else
            if(islower(str[i]))
            {
                ++l_conso;

                while(str[i] == ' ')
                    ++i;
            }
        }
}
