#include<iostream>
#include<cstdlib>
#include<conio.h>

#define ESC '\033'
#define SPACE '\040'

using namespace std;

int main()
{
    system("cls");

    char detect;

    //ESC
    cout << "Press ESC key .... ";
    detect = getche();
    if(detect == ESC)
        cout << "\nESC detected..." << endl;
    else
        cout << "\nNot detected... :( " << endl;
    getch();

    system("cls");
    //BACKSPACE
    cout << "Press BACKSPACE key .... ";
    detect = getche();
    if(detect == '\b')
        cout << "\nBACKSPACE detected..." << endl;
    else
        cout << "\nNot detected... :( " << endl;
    getch();

    system("cls");
    //ENTER
    cout << "Press ENTER key .... ";
    detect = getche();
    if(detect == '\r')
        cout << "\nENTER detected..." << endl;
    else
        cout << "\nNot detected... :( " << endl;
    getch();

    system("cls");
    //SPACE
    cout << "Press SPACE key .... ";
    detect = getche();
    if(detect == SPACE)
        cout << "\nSPACE detected..." << endl;
    else
        cout << "\nNot detected... :( " << endl;
    getch();

    system("cls");
    //TAB
    cout << "Press TAB key .... ";
    detect = getche();
    if(detect == '\t')
        cout << "\nTAB detected..." << endl;
    else
        cout << "\nNot detected... :( " << endl;

    getch();
    return 0;
}
