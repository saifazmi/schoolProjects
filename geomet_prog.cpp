#include <iostream>
#include <cmath>

using namespace std;

int geo_prog(int,int);

int main()
{
    int x, n, ans;
    char choice;

    do{
    cout << "Enter the integer: ";
    cin >> x;
    cout << "Enter the power limit: ";
    cin >> n;

    ans=geo_prog(x,n);
    cout<<ans<<endl;

    cout << "\nWant to Enter more values? (Y/N): ";
    cin >> choice;
    cout <<endl;
    } while (choice == 'Y' || choice == 'y');

    return 0;
}

int geo_prog(int X, int N)
{
    int sum = 0;
    sum = 1+X;

    for (int i=2; i<=N; i++)


        sum = sum + pow(X,i);


    return sum;
}


