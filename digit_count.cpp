#include <iostream>
using namespace std;

int numdigits(int n)
{
int count = 0;
do
{
++count;
n /= 10;
}
while(n != 0);
return count;
}


int main()
{
    int num, digits=0;
    cout << "Enter the number: ";
    cin >> num;
    digits=numdigits(num);
    cout<<"Number of digits = "<<digits;
    return 0;
}
