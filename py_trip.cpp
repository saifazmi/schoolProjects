#include<iostream>

using namespace std;

void description(void);
void pause();

int main()
{
    int side1, side2, hyper, solu=0;

    description();

    cout<<"\nPossible combinations are:\n\n";

        for(side1=100; side1<=1000; side1++)
        {
            for(side2=100; side2<=1000; side2++)
            {
                for(hyper=100; hyper<=1000; hyper++)
                {
                    if((side1*side1)+(side2*side2)==(hyper*hyper))
                    {
                        cout<<side1<<"\t"<<side2<<"\t"<<hyper<<endl;
                        ++solu;

                    if(solu%10 == 0)
                        pause();
                    }
                }
            }
        }

    cout<<"\n There are a total of " << solu <<" possible pythagorean triplets"
                                             <<" from 100 to 1000"<<endl;

    pause();
    return 0;
}

void description(void)
{
    cout<<"This program will print all the possible Pythagorean triplets"
        <<" from 100 to 1000, which means that the three sides of"
        <<" the triangle are more than 100 units and less than 1000 units"<<endl;
}

void pause()
{
    cout<<"\nPress any key to continue...";
    cout<<"\r";
    cout<<"                             ";
    cout<<"\r";
}
