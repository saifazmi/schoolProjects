#include "BILL.h"
#include "INVENTORY.h"
#include <cstring>       //  For handling character strings
#include <cstring>       //  For handling character strings
#include <iostream>      //  For input/output stream
#include <ctime>         //  For sysem time functions
#include <windows.h>      //  For exit();
#include <fstream>      //  For data file handling classes and functions
#include <conio.h>
#include <iomanip>

#define ESC '\033'
#define SPACEBAR '\040'

using namespace std;

/*  Function to add new node
    to linked stack             */
bill_item_Node *Create_New_itemNode(int code, char *name, int quantity, float cost)
{
    ptr = new bill_item_Node;

    ptr -> iCode = code;
    strcpy(ptr -> iName,name);
    ptr -> Qty = quantity;
    ptr -> Price = cost;

    ptr -> next = NULL;

    return ptr;
}

BILL::BILL()
{
    qty = 0;
    price = 0.00;
    totalAmount = 0.00;
}

/*  Function to print current
    system's - date and time    */
void BILL :: sys_date_time()
{
    //for printing system date

    char date[9];
    _strdate(date);
    cout << " " << date << endl;

    //for printing system time

    char time [10];
    _strtime(time);
    cout << " " << time << endl;
}

/*  Function to dynamicaly Add
    bill data to linked stack   */
void BILL :: Push(bill_item_Node *np)
{
    if(top == NULL)
        top = np;

    else
    {
        save = top;
        top = np;

        np -> next = save;
    }
}

/*  Function to Delete last
    entered data in the Bill    */
void BILL :: Pop()
{
    system("cls");

    if(top == NULL)
        cout << "\a\n\tNO MORE ITEMS TO DELETE!!" << endl;

    else
    {
        ptr = top;
        top = top -> next;

        cout << "\a\n\n\t" << ptr -> iName << " removed from Bill !";

        delete ptr;
    }
}

/*  Function to Add item info
    to the Bill                 */
char BILL :: setBill()
{
    char choice;
    top = NULL;

    do
    {
        system("cls");

        char found = 'f';
        long updatePos = 0;

        fstream Inventory;
        Inventory.open("inventory.dat", ios::in | ios::out | ios::binary);

        cout << "\n\n\t\t\t\tBILL" << endl;
        cout << "\t\t\t\t-----\n" << endl;

        cout << "\n Item Code\t:\t";
        cin >> itemCode;

        Inventory.seekg(0);
        Inventory.seekp(0);

        while(!Inventory.eof())
        {
            updatePos = Inventory.tellg();

            Inventory.read((char*) &stockAccess, sizeof(stockAccess));

            if(itemCode == stockAccess.get_proCode())
            {
                if(stockAccess.get_stock() == 0)
                {
                    found = 'o';

                    cout << "\a\n\n Sorry...OUT OF STOCK!!!" << endl;

                    Sleep(1000);
                    cout << "\n\n\n To ADD another item press [ENTER]..." << endl;
                    cout << "\n To PRINT bill press [SPACEBAR]..." << endl;
                    cout << "\n For Main Menu press [BACKSPACE]..." << endl;
                    choice = getche();

                    break;
                }

                else
                {
                    found = 't';

                    strcpy(itemName,stockAccess.get_proName());

                    cout << "\n Quantity\t:\t";
                    cin >> qty;

                    if(qty > stockAccess.get_stock())
                    {
                        cout << "\n\n\a\tOnly " << stockAccess.get_stock() << " in stock";
                        Sleep(2000);

                        qty = stockAccess.get_stock();
                    }

                    price = stockAccess.get_rate() * qty;

                    totalAmount += price;

                    break;
                }
            }
        }

        if(found == 'f')
        {
            cout << "\n Enter a Valid Stock Number !!" << endl;

            cout << "\n\n\n To ADD another item press [ENTER]..." << endl;
            cout << "\n To PRINT bill press [SPACEBAR]..." << endl;
            cout << "\n For Main Menu press [BACKSPACE]..." << endl;
            choice = getche();
        }

        else if(found == 't')
        {
            newptr = Create_New_itemNode(itemCode,itemName,qty,price);

            if(newptr == NULL)
            {
                Sleep(1000);
                cout << "\a\n\n\n Can't add item to bill" << endl;
                cout << "\n STACK UNDERFLOW !!!" << endl;

                cout << "\n\n For Main Menu press [BACKSPACE]..." << endl;
                choice = getche();
            }

            else
            {
                Push(newptr);

                Sleep(1000);

                cout << "\n\n\n\t" << itemName << " added to Bill !" << endl;

                Sleep(3000);

                system("cls");
                cout << "\n To ADD more items press [ENTER]..." << endl;
                cout << "\n To REMOVE Last item from Bill press [TAB]..." << endl;
                cout << "\n To PRINT bill press [SPACEBAR]..." << endl;
                choice = getche();

                if(choice != '\t')
                {
                    stockAccess.set_stock(qty);

                    Inventory.seekp(updatePos);
                    Inventory.write((char*) &stockAccess, sizeof(stockAccess));
                }

                else if(choice == '\t')
                {
                    do
                    {
                        Pop();

                        Sleep(3000);
                        system("cls");

                        cout << "\n To DELETE next item press [TAB] again..." << endl;
                        cout << "\n To ADD more items press [ENTER]..." << endl;
                        cout << "\n To PRINT bill press [SPACEBAR]..." << endl;
                        choice = getche();

                    }   while(choice == '\t');
                }
            }
        }

        if(choice == SPACEBAR)
            choice = displayBill(top);

        Inventory.close();

    } while(choice == '\r');

    return choice;
}

/*  Function to Print the final
    Bill with total amount      */
char BILL :: displayBill(bill_item_Node *np)
{
    system("cls");

    char choice;

    cout << "\n\t\t\tSuper Market Bill" << endl;
    cout << "\t\t\t-----------------" << endl;

    cout << "\n Bill Date and Time - " << endl;
    cout << " ----------------------\n" << endl;
    sys_date_time();
    cout << " ****************************************************" << endl;

    cout << "\n\n ----------------------------------------------------" << endl;
    cout << " Item Code" << "\tItem Name" << "\tQty" << "\tCost" << endl;
    cout << " ----------------------------------------------------\n\n" << endl;

    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout << setprecision(2);

    if(np == NULL)
    {
        cout << "\a NO ITEM ADDED TO THE BILL" << endl;
    }

    else
    {
        while(np != NULL)
        {
            cout << " " << np -> iCode << "\t\t" << np -> iName << "\t\t" << np -> Qty << "\t" << np -> Price << endl;
            np = np -> next;
        }

        cout << " ----------------------------------------------------" << endl;
        cout << "\n\n\t\t\t\tTOTAL = Rs. " << totalAmount << " p." << endl;
    }

    cout << " ****************************************************" << endl;

    Sleep(4000);
    cout << "\n\n To return to Main Menu press [BACKSPACE]...";
    choice =  getche();

    return choice;
}
