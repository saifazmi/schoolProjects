#ifndef MENU_H
#define MENU_H

/*  Class to store Menu
    related data & member funtions  */
class MENU
{
    private:
        //  to store value of in menu selections
        char menuChoice;
        //  choice for main menu
        char mainChoice;
        //  choice after bill print
        char billChoice;

        //  inventory related choices
        char invChoice;
        char invView_choice;
        char invMan_choice;
        char invAdd_choice;
        char invUpdt_choice;
        char invDel_choice;
        char invSearch_choice;

        //  Menu related function prototypes
        char mainMenu();
        char inventoryMenu();
        char inv_manageMenu();

    public:
        MENU();

};

#endif // MENU_H
