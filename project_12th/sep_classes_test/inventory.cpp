#include "INVENTORY.h"
#include <iostream>     //  For input/output stream
#include <cstdlib>      //  For exit();
#include <fstream>      //  For data file handling classes and functions
#include <iomanip>      //  For setprecssion();
#include <conio.h>      //  For getche();
#include <windows.h>    //  For Sleep();

using namespace std;

INVENTORY INVENTORY::INVENTORY()
{
    //Initialising int and float values to prevent errors.
    stock = 0;
    rate = 0.00;
}

int INVENTORY::get_proCode()
{
    return proCode;
}

char* INVENTORY::get_proName()
{
    return proName;
}

/*  Function to update value of
    stock after billing         */
void INVENTORY::set_stock(int q)
{
    stock -= q;
}

int INVENTORY::get_stock()
{
    return stock;
}

float INVENTORY::get_rate()
{
    return rate;
}

/*  Function for the layout
    of the table contents       */
void INVENTORY::tableLayout()
{
    cout << "\n -----------------------------------------------" << endl;
    cout << " Pro. Code" << "\tPro. Name" << "\tStock" << "\tRate" << endl;
    cout << " -----------------------------------------------\n" << endl;
}

/*  Function to DISPLAY
    complete inventory          */
char INVENTORY::displayInventory()
{
    system("cls");

    char choice;

    ifstream inventoryView;
    inventoryView.open("inventory.dat", ios::in | ios::binary);

    cout << "\n\t\t\tINVENTORY" << endl;
    cout << "\t\t\t---------\n" << endl;

    tableLayout();

    if(!inventoryView)
    {
        cout << "\a\n\n\tInventory is EMPTY !!!" << endl;
    }

    else
    {
        cout.setf(ios::fixed);
        cout.setf(ios::showpoint);
        cout << setprecision(2);

        inventoryView.seekg(0);

        /*  To avoid displaying duplicate
            records at the end of file      */
        long end = (-1*sizeof(stockView), ios::end);
        long pos = 0;

        while(pos!=end && inventoryView.read((char*) &stockView, sizeof(stockView)))
        {
            cout << " " << proCode << "\t\t" << proName << "\t\t" << stock << "\t" << rate << endl;
        }
    }

    cout << "\n\n For Inventory Menu press [BACKSPACE]..." << endl;
    choice = getche();

    inventoryView.close();

    return choice;
}

/*  Function to ADD new items
    to the inventory            */
char INVENTORY::setInventory()
{
	char choice;

	do
	{
	    ofstream inventoryAdd;
        inventoryAdd.open("inventory.dat", ios::out | ios::binary | ios::app);

	    system("cls");

	    cout << "\n\t\t\tADD ITEM" << endl;
	    cout << "\t\t\t---------\n" << endl;

        cout << "\n Enter Item Details - " << endl;
        cout << " --------------------"  << endl;

		cout << "\n Code\t:\t";	        cin >> proCode;
		cout << "\n Name\t:\t";	        cin >> proName;
		cout << "\n Qty\t:\t";	        cin >> stock;
		cout << "\n Price\t:\t";	        cin >> rate;

		inventoryAdd.write((char*) &stockUpdate, sizeof(stockUpdate));

        Sleep(1000);
		cout << "\n\n Item sucessfully added !" << endl;

        Sleep(2000);
		system("cls");

		cout << "\n To ADD another item press [TAB]..." << endl;
		cout << "\n For Inventory Manage. Menu press [BACKSPACE]..." << endl;
		choice = getche();

		inventoryAdd.close();

	} while(choice == '\t');

	return choice;
}

/*  Function to UPDATE existing
    items in the inventory      */
char INVENTORY::updateInventory()
{
    system("cls");

    char choice;

    do
    {
        int code = 0;
        char found = 'f';
        long updatePos = 0;

        fstream inventoryUpdate;
        inventoryUpdate.open("inventory.dat", ios::in | ios::out | ios::binary);

        system("cls");

        cout << "\n\t\t\tUPDATE INVENTORY" << endl;
        cout << "\t\t\t-----------------" << endl;

        cout << "\n\n Enter Item Code\t:\t";
        cin >> code;

        inventoryUpdate.seekg(0);

        while(!inventoryUpdate.eof())
        {
            updatePos = inventoryUpdate.tellg();

            inventoryUpdate.read((char*) &stockUpdate, sizeof(stockUpdate));
            if(code == proCode)
            {
                found = 't';
                char ch;

                cout << "\n Product\t\t:\t" << proName << endl;
                cout << "\n\n Update QTY ? Press [Q]..." << endl;
                cout << "\n Update PRICE ? Press [P]..." << endl;
                ch = getche();

                system("cls");

                cout << "\n\t\t\tUPDATE " << proName << endl;
                cout << "\t\t\t-----------------\n" << endl;

                if(ch == 'q' || ch == 'Q')
                {
                    int qty = 0;

                    cout << "\n Enter Qty to ADD\t:\t";
                    cin >> qty;

                    stock += qty;

                    inventoryUpdate.seekp(updatePos);
                    inventoryUpdate.write((char*) &stockUpdate, sizeof(stockUpdate));
                }

                else if(ch == 'p' || ch == 'P')
                {
                    float price = 0.00;

                    cout << "\n Enter NEW Price\t:\tRs. ";
                    cin >> price;

                    rate = price;

                    inventoryUpdate.seekp(updatePos);
                    inventoryUpdate.write((char*) &stockUpdate, sizeof(stockUpdate));
                }

                break;
            }
        }

        Sleep(1000);

        if(found == 'f')
        {
            cout << "\a\n\n Item NOT found in inventory !!!" << endl;
        }

        else if(found = 't')
        {
            cout << "\a\n\n Item Sucessfully Updated !!!" << endl;

            Sleep(2000);

            system("cls");

            cout << "\n Item after Updation - " << endl;
            tableLayout();

            cout.setf(ios::fixed);
            cout.setf(ios::showpoint);
            cout << setprecision(2);

            cout << " " << proCode << "\t\t" << proName << "\t\t" << stock << "\t" << rate << endl;
        }

        Sleep(1000);

        cout << "\n\n\n\n\n To UPDATE another item press [ENTER]..." << endl;
        cout << "\n For Inventory Manage. Menu press [BACKSAPCE]..." << endl;
        choice = getche();

        inventoryUpdate.close();

    }   while(choice == '\r');

    return choice;
}

/*  Function to DELETE an item
    from the inventory          */
char INVENTORY::delInventory()
{
    char choice;

    do
    {
        system("cls");

        int code = 0;
        char found = 'f';
        char confirm = 'n';

        ifstream inventoryDel;
        inventoryDel.open("inventory.dat", ios::in | ios::binary);

        ofstream inventoryDel_temp;
        inventoryDel_temp.open("temp.dat", ios::out | ios::binary);

        cout << "\n\t\t\tDELETE ITEM" << endl;
        cout << "\t\t\t------------" << endl;

        cout << "\n\n Item code\t:\t";
        cin >> code;

        inventoryDel.seekg(0);
        inventoryDel_temp.seekp(0);

        long end = (-1*sizeof(stockUpdate), ios::end);
        long pos = 0;

        while(pos!=end && inventoryDel.read((char*) &stockUpdate, sizeof(stockUpdate)))
        {
            if(code == proCode)
            {
                found = 't';

                cout.setf(ios::fixed);
                cout.setf(ios::showpoint);
                cout << setprecision(2);

                cout << "\n Name\t\t:\t"          << proName;
                cout << "\n Qty\t\t:\t"           << stock;
                cout << "\n Price\t\t:\t"         << rate;

                cout << "\n\n Do you want to delete this item? [Y/N]...";
                confirm = getche();

                if(confirm == 'n' || confirm == 'N')
                {
                    inventoryDel_temp.write((char*) &stockUpdate, sizeof(stockUpdate));
                    cout << "\n\n User choose NOT to Delete the item above." << endl;
                }
            }

            else
                inventoryDel_temp.write((char*) &stockUpdate, sizeof(stockUpdate));
        }

        Sleep(1000);

        if(found == 'f')
            cout << "\a\n\n\n Item DOES NOT EXSISTS in the Inventory!!!" << endl;

        else if(found == 't' && (confirm == 'y' || confirm == 'Y'))
            cout << "\a\n\n Item details Sucessfull Deleted from Inventory !!!" << endl;

        inventoryDel.close();
        inventoryDel_temp.close();

        remove("inventory.dat");
        rename("temp.dat","inventory.dat");

        Sleep(2000);
        system("cls");

        cout << "\n To DELETE another item press [TAB]..." << endl;
        cout << "\n For Inventory Manage. Menu press [BACKSAPCE]..." << endl;
        choice = getche();

    }   while(choice == '\t');

    return choice;
}

/*  Function to SEARCH for an
    item in the inventory       */
char INVENTORY::searchInventory()
{
    char choice;

    do
    {
        system("cls");

        ifstream inventorySearch;
        inventorySearch.open("inventory.dat", ios::in | ios:: binary);

        int code = 0;
        char found = 'f';

        cout << "\n\t\t\tInventory Search" << endl;
        cout << "\t\t\t-----------------" << endl;

        cout << "\n\n Enter Item Code\t:\t";
        cin >> code;

        inventorySearch.seekg(0);

        while(!inventorySearch.eof())
        {
            inventorySearch.read((char*) &stockView, sizeof(stockView));
            if(code == proCode)
            {
                found = 't';

                tableLayout();

                cout.setf(ios::fixed);
                cout.setf(ios::showpoint);
                cout << setprecision(2);

                cout << " " << proCode << "\t\t" << proName << "\t\t" << stock << "\t" << rate << endl;

                break;
            }
        }

        Sleep(1000);

        if(found == 'f')
        {
            cout << "\a\n\n Item NOT found in inventory !!!" << endl;
        }

        else if(found == 't')
        {
            cout << "\n\n Item Found!!!" << endl;
        }

        Sleep(1000);

        cout << "\n\n\n To SEARCH for another item press [ENTER]..." << endl;
        cout << "\n For Inventory Menu press [BACKSPACE]..." << endl;
        choice = getche();

        inventorySearch.close();

    }   while(choice == '\r');

    return choice;
}
