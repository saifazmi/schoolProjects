#ifndef INVENTORY_H
#define INVENTORY_H

/*  Class to store Inventory
    related data & member funtions  */

class INVENTORY
{
	private:
		int proCode;
		char proName[60];
		int stock;
		float rate;
		void tableLayout();

	public:
	    INVENTORY();
        int get_proCode();
        char* get_proName();
        /*  Function to update value of
            stock after billing         */
        void set_stock(int q);
        int get_stock();
        float get_rate();

        //Function prototypes
        char displayInventory();

	    char setInventory();
	    char updateInventory();
	    char delInventory();

	    char searchInventory();

}   stockUpdate, stockView, stockAccess;

#endif // INVENTORY_H
