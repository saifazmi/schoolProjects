#ifndef BILL_H
#define BILL_H

/*  Structure to store billing
    related data members        */
struct bill_item_Node
{
    int iCode;
    char iName[60];
    int Qty;
    float Price;
    bill_item_Node *next;

}   *top, *newptr, *save, *ptr;

/*  Class to store Billing
    related data & member funtions  */
class BILL
{
    private:
		int itemCode;
		char itemName[60];
		int qty;
		float price;
		float totalAmount;

        void sys_date_time();
		void Push(bill_item_Node *);
		void Pop();
		char displayBill(bill_item_Node *);

	public:
        BILL();

        char setBill();

}   billItem;

#endif // BILL_H
