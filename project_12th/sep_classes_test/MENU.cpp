#include "MENU.h"
#include "BILL.h"
#include "INVENTORY.h"
#include <iostream>     //  For input/output stream
#include <windows.h>    //  For Sleep();
#include <conio.h>      //  For getche();

//  Assigning octal key values to variables
#define ESC '\033'
#define SPACEBAR '\040'

using namespace std;

/*  Constructor for class Menu it has
    the logic for traversing the menu   */
MENU :: MENU()
{
    //Introductory Screen
    cout << "\n\tCOMPUTER SCIENCE(C++) PROJECT - 083" << endl;

    cout << "\n\tDelhi Public School," << endl;
    cout << "\tSector - 19, Indira Nagar, Lucknow." << endl;

    cout << "\n\n\tSUPER MARKET MANAGEMENT" << endl;
    cout << "\t-----------------------" << endl;
    cout << "\n\tAND" << endl;
    cout << "\t---" << endl;
    cout << "\n\tBILLING SYSTEM" << endl;
    cout << "\t--------------" << endl;

    cout << "\n\n\tDEVELOPED, COMPILED AND DESIGNED BY - " << endl;
    cout << "\n\t\t# Alka Rao" << endl;
    cout << "\t\t# Saifullah Azmi" << endl;
    cout << "\t\t# Varun Verma" << endl;

    Sleep(4000);

    do
    {
        mainChoice = mainMenu();

        //NEW BILL
        if(mainChoice == 'N' || mainChoice == 'n')
        {
            billChoice = billItem.setBill();
        }

        //INVENTORY
        else if(mainChoice == 'I' || mainChoice == 'i')
        {
            do
            {
                invChoice = inventoryMenu();
                if(invChoice == '\b')
                    break;

                //VIEW INVENTORY
                if(invChoice == 'V' || invChoice == 'v')
                {
                    invView_choice = stockView.displayInventory();
                }

                //MODIFY INVENTORY
                else if(invChoice == 'M' || invChoice == 'm')
                {
                    do
                    {
                        invMan_choice = inv_manageMenu();
                        if(invMan_choice == '\b')
                            break;

                        //ADD ITEM
                        if(invMan_choice == 'A' || invMan_choice == 'a')
                        {
                            invAdd_choice = stockUpdate.setInventory();
                        }

                        //UPDATE ITEM
                        else if(invMan_choice == 'U' || invMan_choice == 'u')
                        {
                            invUpdt_choice = stockUpdate.updateInventory();
                        }

                        //DELETE ITEM
                        else if(invMan_choice == 'D' || invMan_choice == 'd')
                        {
                            invDel_choice = stockUpdate.delInventory();
                        }

                    }   while(invAdd_choice == '\b' || invUpdt_choice == '\b' || invDel_choice == '\b');
                }

                //SEARCH INVENTORY
                else if(invChoice == 'S' || invChoice == 's')
                {
                    invSearch_choice = stockView.searchInventory();
                }

            }   while(invView_choice == '\b' || invMan_choice == '\b' || invSearch_choice == '\b');
        }

        //EXIT SUPER MARKET MANAGER
        else if(mainChoice == ESC)
        {
            system("cls");

            cout << "\n\n\n\n\n\n\t\t\t\tThank You for using" << endl;
            cout << "\n\n\t\t\t\tSUPER MARKET MANAGER" << endl;
            cout << "\n\n\t\t\t\tHave a nice Day ^_^" << endl;

            Sleep(3000);
            exit(0);
        }

    }   while(billChoice == '\b' || invChoice == '\b');
}

/*  Function containing
    options for Main Menu       */
char MENU :: mainMenu()
{
    system("cls");

    cout << "\n\t\t\t\tSUPER MARKET MANAGER" << endl;
    cout << "\t\t\t\t--------------------" << endl;

    cout << "\n\n New Bill.\t\t[N]" << endl;
    cout << " Inventory.\t\t[I]" << endl;
    cout << " Exit.\t\t\t[ESC]" << endl;

    cout << "\n Press a key to proceed...";
    menuChoice = getche();

    return menuChoice;
}

/*  Function containing
    options for Inventory Menu   */
char MENU :: inventoryMenu()
{
    system("cls");

    cout << "\n\t\t\tINVENTORY" << endl;
    cout << "\t\t\t---------" << endl;

    cout << "\n\n VIEW Inventory.\t[V]" << endl;
    cout << " MANAGE Inventory.\t[M]" << endl;
    cout << " SEARCH Inventory.\t[S]" << endl;
    cout << " BACK -> Main Menu.\t[BACKSPACE]" << endl;

    cout << "\n Press a key to proceed...";
    menuChoice = getche();

    return menuChoice;
}

/*  Function containing options
    for Inventory Managment Menu */
char MENU :: inv_manageMenu()
{
    system("cls");

    cout << "\n\t\t\tINVENTORY MANAGMENT" << endl;
    cout << "\t\t\t-------------------" << endl;

    cout << "\n\n ADD Item.\t\t[A]" << endl;
    cout << " UPDATE Item.\t\t[U]" << endl;
    cout << " DELETE Item.\t\t[D]" << endl;
    cout << " BACK -> Inventory.\t[BACKSPACE]" << endl;

    cout << "\n Press a key to proceed...";
    menuChoice = getche();

    return menuChoice;
}
