//debug mode

#include<iostream>      //  For input/output stream
#include<fstream>       //  For data file handling classes and functions
//#include<conio.h>       //  For clrscr(); and getche();
#include<cstdio>        //  For standard I/O functions
#include<iomanip>       //  For setprecssion();
#include<ctime>         //  For sysem time functions
#include<cstring>       //  For handling character strings
#include<cstdlib>       //  For exit();

//  Assigning octal key values to variables
#define ESC '\033'
#define SPACEBAR '\040'

using namespace std;

class INVENTORY
{
	private:
		int proCode;
		char proName[60];
		int stock;
		float rate;

	public:
	    INVENTORY()
	    {
	        //Initialising int and float values to prevent errors.
            stock = 0;
            rate = 0.00;
	    }

	    int get_proCode()
	    {
	        return proCode;
	    }

        char* get_proName()
        {
            return proName;
        }

        /*  Function to update value of
            stock after billing         */
        void set_stock(int q)
        {
            stock -= q;
        }

        int get_stock()
        {
            return stock;
        }

        float get_rate()
        {
            return rate;
        }

        //Function prototypes
        char displayInventory();

	    char setInventory();
	    char updateInventory();
	    char delInventory();

	    char searchInventory();

}   stockUpdate, stockView, stockAccess;

/*  Function to DISPLAY
    complete inventory          */
char INVENTORY :: displayInventory()
{
    system("cls");

    char choice;

    ifstream inventoryView;
    inventoryView.open("inventory.dat", ios::in | ios::binary);

    cout << "\n\t\t\tINVENTORY" << endl;
    cout << "\t\t\t---------\n" << endl;

    cout << "\n -----------------------------------------------" << endl;
    cout << " Pro. Code" << "\tPro. Name" << "\tStock" << "\tRate" << endl;
    cout << " -----------------------------------------------\n" << endl;

    if(!inventoryView)
    {
        cout << "\a\n\n\tInventory is EMPTY !!!" << endl;
    }

    else
    {
        cout.setf(ios::fixed);
        cout.setf(ios::showpoint);
        cout << setprecision(2);

        inventoryView.seekg(0);

        /*  To avoid displaying duplicate
            records at the end of file      */
        long end = (-1*sizeof(stockView), ios::end);
        long pos = 0;

        while(pos!=end && inventoryView.read((char*) &stockView, sizeof(stockView)))
        {
            cout << " " << proCode << "\t\t" << proName << "\t\t" << stock << "\t" << rate << endl;
        }
    }

    cout << "\n\n For Inventory Menu press [BACKSPACE]..." << endl;
    choice = getche();

    inventoryView.close();

    return choice;
}

/*  Function to ADD new items
    to the inventory            */
char INVENTORY :: setInventory()
{
	char choice;

	do
	{
	    ofstream inventoryAdd;
        inventoryAdd.open("inventory.dat", ios::out | ios::binary | ios::app);

	    system("cls");

	    cout << "\n\t\t\tADD ITEM" << endl;
	    cout << "\t\t\t---------\n" << endl;

        cout << "\n Enter Item Details - " << endl;
        cout << " --------------------"  << endl;

		cout << "\n Code\t:\t";	        cin >> proCode;
		cout << "\n Name\t:\t";	        cin >> proName;
		cout << "\n Qty\t:\t";	        cin >> stock;
		cout << "\n Price\t:\t";	    cin >> rate;

		inventoryAdd.write((char*) &stockUpdate, sizeof(stockUpdate));

        Sleep(1000);
		cout << "\n\n Item sucessfully added !" << endl;

        Sleep(2000);
		system("cls");

		cout << "\n To ADD another item press [TAB]..." << endl;
		cout << "\n For Inventory Manage. Menu press [BACKSPACE]..." << endl;
		choice = getche();

		inventoryAdd.close();

	} while(choice == '\t');

	return choice;
}

/*  Function to UPDATE existing
    items in the inventory      */
char INVENTORY :: updateInventory()
{
    system("cls");

    char choice;

    do
    {
        int code = 0;
        char found = 'f';
        long updatePos = 0;

        fstream inventoryUpdate;
        inventoryUpdate.open("inventory.dat", ios::in | ios::out | ios::binary);

        system("cls");

        cout << "\n\t\t\tUPDATE INVENTORY" << endl;
        cout << "\t\t\t-----------------" << endl;

        cout << "\n\n Enter Item Code\t:\t";
        cin >> code;

        inventoryUpdate.seekg(0);

        while(!inventoryUpdate.eof())
        {
            updatePos = inventoryUpdate.tellg();

            inventoryUpdate.read((char*) &stockUpdate, sizeof(stockUpdate));
            if(code == proCode)
            {
                found = 't';
                char ch;

                cout << "\n Product\t\t:\t" << proName << endl;
                cout << "\n\n Update QTY ? Press [Q]..." << endl;
                cout << "\n Update PRICE ? Press [P]..." << endl;
                ch = getche();

                system("cls");

                cout << "\n\t\t\tUPDATE " << proName << endl;
                cout << "\t\t\t-----------------\n" << endl;

                if(ch == 'q' || ch == 'Q')
                {
                    int qty = 0;

                    cout << "\n Enter Qty to ADD\t:\t";
                    cin >> qty;

                    stock += qty;

                    inventoryUpdate.seekp(updatePos);
                    inventoryUpdate.write((char*) &stockUpdate, sizeof(stockUpdate));
                }

                else if(ch == 'p' || ch == 'P')
                {
                    float price = 0.00;

                    cout << "\n Enter NEW Price\t:\tRs. ";
                    cin >> price;

                    rate = price;

                    inventoryUpdate.seekp(updatePos);
                    inventoryUpdate.write((char*) &stockUpdate, sizeof(stockUpdate));
                }

                break;
            }
        }

        Sleep(1000);

        if(found == 'f')
        {
            cout << "\a\n\n Item NOT found in inventory !!!" << endl;
        }

        else if(found = 't')
        {
            cout << "\a\n\n Item Sucessfully Updated !!!" << endl;

            Sleep(2000);

            system("cls");

            cout << "\n Item after Updation - " << endl;
            cout << "\n -------------------------------------------------" << endl;
            cout << " Pro. Code" << "\tPro. Name" << "\tStock" << "\tRate" << endl;
            cout << " -------------------------------------------------\n" << endl;

            cout.setf(ios::fixed);
            cout.setf(ios::showpoint);
            cout << setprecision(2);

            cout << " " << proCode << "\t\t" << proName << "\t\t" << stock << "\t" << rate << endl;
        }

        Sleep(1000);

        cout << "\n\n\n\n\n To UPDATE another item press [ENTER]..." << endl;
        cout << "\n For Inventory Manage. Menu press [BACKSAPCE]..." << endl;
        choice = getche();

        inventoryUpdate.close();

    }   while(choice == '\r');

    return choice;
}

/*  Function to DELETE an item
    from the inventory          */
char INVENTORY :: delInventory()
{
    char choice;

    do
    {
        system("cls");

        int code = 0;
        char found = 'f';
        char confirm = 'n';

        ifstream inventoryDel;
        inventoryDel.open("inventory.dat", ios::in | ios::binary);

        ofstream inventoryDel_temp;
        inventoryDel_temp.open("temp.dat", ios::out | ios::binary);

        cout << "\n\t\t\tDELETE ITEM" << endl;
        cout << "\t\t\t------------" << endl;

        cout << "\n\n Item code\t:\t";
        cin >> code;

        inventoryDel.seekg(0);
        inventoryDel_temp.seekp(0);

        long end = (-1*sizeof(stockUpdate), ios::end);
        long pos = 0;

        while(pos!=end && inventoryDel.read((char*) &stockUpdate, sizeof(stockUpdate)))
        {
            if(code == proCode)
            {
                found = 't';

                cout.setf(ios::fixed);
                cout.setf(ios::showpoint);
                cout << setprecision(2);

                cout << "\n Name\t\t:\t"          << proName;
                cout << "\n Qty\t\t:\t"           << stock;
                cout << "\n Price\t\t:\t"         << rate;

                cout << "\n\n Do you want to delete this item? [Y/N]...";
                confirm = getche();

                if(confirm == 'n' || confirm == 'N')
                {
                    inventoryDel_temp.write((char*) &stockUpdate, sizeof(stockUpdate));
                    cout << "\n\n User choose NOT to Delete the item above." << endl;
                }
            }

            else
                inventoryDel_temp.write((char*) &stockUpdate, sizeof(stockUpdate));
        }

        Sleep(1000);

        if(found == 'f')
            cout << "\a\n\n\n Item DOES NOT EXSISTS in the Inventory!!!" << endl;

        else if(found == 't' && (confirm == 'y' || confirm == 'Y'))
            cout << "\a\n\n Item details Sucessfull Deleted from Inventory !!!" << endl;

        inventoryDel.close();
        inventoryDel_temp.close();

        remove("inventory.dat");
        rename("temp.dat","inventory.dat");

        Sleep(2000);
        system("cls");

        cout << "\n To DELETE another item press [TAB]..." << endl;
        cout << "\n For Inventory Manage. Menu press [BACKSAPCE]..." << endl;
        choice = getche();

    }   while(choice == '\t');

    return choice;
}

/*  Function to SEARCH for an
    item in the inventory       */
char INVENTORY :: searchInventory()
{
    char choice;

    do
    {
        system("cls");

        ifstream inventorySearch;
        inventorySearch.open("inventory.dat", ios::in | ios:: binary);

        int code = 0;
        char found = 'f';

        cout << "\n\t\t\tInventory Search" << endl;
        cout << "\t\t\t-----------------" << endl;

        cout << "\n\n Enter Item Code\t:\t";
        cin >> code;

        inventorySearch.seekg(0);

        while(!inventorySearch.eof())
        {
            inventorySearch.read((char*) &stockView, sizeof(stockView));
            if(code == proCode)
            {
                found = 't';

                cout << "\n -------------------------------------------------" << endl;
                cout << " Pro. Code" << "\tPro. Name" << "\tStock" << "\tRate" << endl;
                cout << " -------------------------------------------------\n" << endl;

                cout.setf(ios::fixed);
                cout.setf(ios::showpoint);
                cout << setprecision(2);

                cout << " " << proCode << "\t\t" << proName << "\t\t" << stock << "\t" << rate << endl;

                break;
            }
        }

        Sleep(1000);

        if(found == 'f')
        {
            cout << "\a\n\n Item NOT found in inventory !!!" << endl;
        }

        else if(found == 't')
        {
            cout << "\n\n Item Found!!!" << endl;
        }

        Sleep(1000);

        cout << "\n\n\n To SEARCH for another item press [ENTER]..." << endl;
        cout << "\n For Inventory Menu press [BACKSPACE]..." << endl;
        choice = getche();

        inventorySearch.close();

    }   while(choice == '\r');

    return choice;
}

/*  Structure to store billing
    related data members        */
struct bill_item_Node
{
    int iCode;
    char iName[60];
    int Qty;
    float Price;
    bill_item_Node *next;

}	*top, *newptr, *save, *ptr;

/*  Function to add new node
    to linked stack             */
bill_item_Node *Create_New_itemNode(int code, char *name, int quantity, float cost)
{
    ptr = new bill_item_Node;

    ptr -> iCode = code;
    strcpy(ptr -> iName,name);
    ptr -> Qty = quantity;
    ptr -> Price = cost;

    ptr -> next = NULL;

    return ptr;
}

/*  Class to store Billing
    related data & member funtions  */
class BILL
{
    private:
		int itemCode;
		char itemName[60];
		int qty;
		float price;
		float totalAmount;

        void sys_date_time();
		void Push(bill_item_Node *);
		void Pop();
		char displayBill(bill_item_Node *);

	public:
        BILL()
        {
            qty = 0;
            price = 0.00;
            totalAmount = 0.00;
        }

        char setBill();

}   billItem;

/*  Function to print current
    system's - date and time    */
void BILL :: sys_date_time()
{
    //for printing system date

    char date[9];
    _strdate(date);
    cout << " " << date << endl;

    //for printing system time

    char time [10];
    _strtime(time);
    cout << " " << time << endl;
}

/*  Function to dynamicaly Add
    bill data to linked stack   */
void BILL :: Push(bill_item_Node *np)
{
    if(top == NULL)
        top = np;

    else
    {
        save = top;
        top = np;

        np -> next = save;
    }
}

/*  Function to Delete last
    entered data in the Bill    */
void BILL :: Pop()
{
    system("cls");

    if(top == NULL)
        cout << "\a\n\tNO MORE ITEMS TO DELETE!!" << endl;

    else
    {
        ptr = top;
        top = top -> next;

        cout << "\a\n\n\t" << ptr -> iName << " removed from Bill !";

        delete ptr;
    }
}

/*  Function to Add item info
    to the Bill                 */
char BILL :: setBill()
{
    char choice;
    top = NULL;

    do
    {
        system("cls");

        char found = 'f';
        long updatePos = 0;

        fstream Inventory;
        Inventory.open("inventory.dat", ios::in | ios::out | ios::binary);

        cout << "\n\n\t\t\t\tBILL" << endl;
        cout << "\t\t\t\t-----\n" << endl;

        cout << "\n Item Code\t:\t";
        cin >> itemCode;

        Inventory.seekg(0);
        Inventory.seekp(0);

        while(!Inventory.eof())
        {
            updatePos = Inventory.tellg();

            Inventory.read((char*) &stockAccess, sizeof(stockAccess));

            if(itemCode == stockAccess.get_proCode())
            {
                if(stockAccess.get_stock() == 0)
                {
                    found = 'o';

                    cout << "\a\n\n Sorry...OUT OF STOCK!!!" << endl;

                    Sleep(1000);
                    cout << "\n\n\n To ADD another item press [ENTER]..." << endl;
                    cout << "\n To PRINT bill press [SPACEBAR]..." << endl;
                    cout << "\n For Main Menu press [BACKSPACE]..." << endl;
                    choice = getche();

                    break;
                }

                else
                {
                    found = 't';

                    strcpy(itemName,stockAccess.get_proName());

                    cout << "\n Quantity\t:\t";
                    cin >> qty;

                    if(qty > stockAccess.get_stock())
                    {
                        cout << "\n\n\a\tOnly " << stockAccess.get_stock() << " in stock";
                        Sleep(2000);

                        qty = stockAccess.get_stock();
                    }

                    price = stockAccess.get_rate() * qty;

                    totalAmount += price;

                    break;
                }
            }
        }

        if(found == 'f')
        {
            cout << "\n Enter a Valid Stock Number !!" << endl;

            cout << "\n\n\n To ADD another item press [ENTER]..." << endl;
            cout << "\n To PRINT bill press [SPACEBAR]..." << endl;
            cout << "\n For Main Menu press [BACKSPACE]..." << endl;
            choice = getche();
        }

        else if(found == 't')
        {
            newptr = Create_New_itemNode(itemCode,itemName,qty,price);

            if(newptr == NULL)
            {
                Sleep(1000);
                cout << "\a\n\n\n Can't add item to bill" << endl;
                cout << "\n STACK UNDERFLOW !!!" << endl;

                cout << "\n\n For Main Menu press [BACKSPACE]..." << endl;
                choice = getche();
            }

            else
            {
                Push(newptr);

                Sleep(1000);

                cout << "\n\n\n\t" << itemName << " added to Bill !" << endl;

                Sleep(3000);

                system("cls");
                cout << "\n To ADD more items press [ENTER]..." << endl;
                cout << "\n To REMOVE Last item from Bill press [TAB]..." << endl;
                cout << "\n To PRINT bill press [SPACEBAR]..." << endl;
                choice = getche();

                if(choice != '\t')
                {
                    stockAccess.set_stock(qty);

                    Inventory.seekp(updatePos);
                    Inventory.write((char*) &stockAccess, sizeof(stockAccess));
                }

                else if(choice == '\t')
                {
                    do
                    {
                        Pop();

                        Sleep(3000);
                        system("cls");

                        cout << "\n To DELETE next item press [TAB] again..." << endl;
                        cout << "\n To ADD more items press [ENTER]..." << endl;
                        cout << "\n To PRINT bill press [SPACEBAR]..." << endl;
                        choice = getche();

                    }   while(choice == '\t');
                }
            }
        }

        if(choice == SPACEBAR)
            choice = displayBill(top);

        Inventory.close();

    } while(choice == '\r');

    return choice;
}

/*  Function to Print the final
    Bill with total amount      */
char BILL :: displayBill(bill_item_Node *np)
{
    system("cls");

    char choice;

    cout << "\n\t\t\tSuper Market Bill" << endl;
    cout << "\t\t\t-----------------" << endl;

    cout << "\n Bill Date and Time - " << endl;
    cout << " ----------------------\n" << endl;
    sys_date_time();
    cout << " ****************************************************" << endl;

    cout << "\n\n ----------------------------------------------------" << endl;
    cout << " Item Code" << "\tItem Name" << "\tQty" << "\tCost" << endl;
    cout << " ----------------------------------------------------\n\n" << endl;

    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout << setprecision(2);

    if(np == NULL)
    {
        cout << "\a NO ITEM ADDED TO THE BILL" << endl;
    }

    else
    {
        while(np != NULL)
        {
            cout << " " << np -> iCode << "\t\t" << np -> iName << "\t\t" << np -> Qty << "\t" << np -> Price << endl;
            np = np -> next;
        }

        cout << " ----------------------------------------------------" << endl;
        cout << "\n\n\t\t\t\tTOTAL = Rs. " << totalAmount << " p." << endl;
    }

    cout << " ****************************************************" << endl;

    Sleep(4000);
    cout << "\n\n To return to Main Menu press [BACKSPACE]...";
    choice =  getche();

    return choice;
}

/*  Class to store Menu
    related data & member funtions  */
class MENU
{
    private:
        //  to store value of in menu selections
        char menuChoice;
        //  choice for main menu
        char mainChoice;
        //  choice after bill print
        char billChoice;

        //  inventory related choices
        char invChoice;
        char invView_choice;
        char invMan_choice;
        char invAdd_choice;
        char invUpdt_choice;
        char invDel_choice;
        char invSearch_choice;

        //  Menu related function prototypes
        char mainMenu();
        char inventoryMenu();
        char inv_manageMenu();

    public:
        MENU();

};

/*  Constructor for class Menu it has
    the logic for traversing the menu   */
MENU :: MENU()
{
    //Introductory Screen
    cout << "\n\tCOMPUTER SCIENCE(C++) PROJECT - 083" << endl;

    cout << "\n\tDelhi Public School," << endl;
    cout << "\tSector - 19, Indira Nagar, Lucknow." << endl;

    cout << "\n\n\tSUPER MARKET MANAGEMENT" << endl;
    cout << "\t-----------------------" << endl;
    cout << "\n\tAND" << endl;
    cout << "\t---" << endl;
    cout << "\n\tBILLING SYSTEM" << endl;
    cout << "\t--------------" << endl;

    cout << "\n\n\tDEVELOPED, COMPILED AND DESIGNED BY - " << endl;
    cout << "\n\t\t# Alka Rao" << endl;
    cout << "\t\t# Saifullah Azmi" << endl;
    cout << "\t\t# Varun Verma" << endl;

    Sleep(4000);

    do
    {
        mainChoice = mainMenu();

        //NEW BILL
        if(mainChoice == 'N' || mainChoice == 'n')
        {
            billChoice = billItem.setBill();
        }

        //INVENTORY
        else if(mainChoice == 'I' || mainChoice == 'i')
        {
            do
            {
                invChoice = inventoryMenu();
                if(invChoice == '\b')
                    break;

                //VIEW INVENTORY
                if(invChoice == 'V' || invChoice == 'v')
                {
                    invView_choice = stockView.displayInventory();
                }

                //MODIFY INVENTORY
                else if(invChoice == 'M' || invChoice == 'm')
                {
                    do
                    {
                        invMan_choice = inv_manageMenu();
                        if(invMan_choice == '\b')
                            break;

                        //ADD ITEM
                        if(invMan_choice == 'A' || invMan_choice == 'a')
                        {
                            invAdd_choice = stockUpdate.setInventory();
                        }

                        //UPDATE ITEM
                        else if(invMan_choice == 'U' || invMan_choice == 'u')
                        {
                            invUpdt_choice = stockUpdate.updateInventory();
                        }

                        //DELETE ITEM
                        else if(invMan_choice == 'D' || invMan_choice == 'd')
                        {
                            invDel_choice = stockUpdate.delInventory();
                        }

                    }   while(invAdd_choice == '\b' || invUpdt_choice == '\b' || invDel_choice == '\b');
                }

                //SEARCH INVENTORY
                else if(invChoice == 'S' || invChoice == 's')
                {
                    invSearch_choice = stockView.searchInventory();
                }

            }   while(invView_choice == '\b' || invMan_choice == '\b' || invSearch_choice == '\b');
        }

        //EXIT SUPER MARKET MANAGER
        else if(mainChoice == ESC)
        {
            system("cls");

            cout << "\n\n\n\n\n\n\t\t\t\tThank You for using" << endl;
            cout << "\n\n\t\t\t\tSUPER MARKET MANAGER" << endl;
            cout << "\n\n\t\t\t\tHave a nice Day ^_^" << endl;

            Sleep(3000);
            exit(0);
        }

    }   while(billChoice == '\b' || invChoice == '\b');
}

/*  Function containing
    options for Main Menu       */
char MENU :: mainMenu()
{
    system("cls");

    cout << "\n\t\t\t\tSUPER MARKET MANAGER" << endl;
    cout << "\t\t\t\t--------------------" << endl;

    cout << "\n\n New Bill.\t\t[N]" << endl;
    cout << " Inventory.\t\t[I]" << endl;
    cout << " Exit.\t\t\t[ESC]" << endl;

    cout << "\n Press a key to proceed...";
    menuChoice = getche();

    return menuChoice;
}

/*  Function containing
    options for Inventory Menu   */
char MENU :: inventoryMenu()
{
    system("cls");

    cout << "\n\t\t\tINVENTORY" << endl;
    cout << "\t\t\t---------" << endl;

    cout << "\n\n VIEW Inventory.\t[V]" << endl;
    cout << " MANAGE Inventory.\t[M]" << endl;
    cout << " SEARCH Inventory.\t[S]" << endl;
    cout << " BACK -> Main Menu.\t[BACKSPACE]" << endl;

    cout << "\n Press a key to proceed...";
    menuChoice = getche();

    return menuChoice;
}

/*  Function containing options
    for Inventory Managment Menu */
char MENU :: inv_manageMenu()
{
    system("cls");

    cout << "\n\t\t\tINVENTORY MANAGMENT" << endl;
    cout << "\t\t\t-------------------" << endl;

    cout << "\n\n ADD Item.\t\t[A]" << endl;
    cout << " UPDATE Item.\t\t[U]" << endl;
    cout << " DELETE Item.\t\t[D]" << endl;
    cout << " BACK -> Inventory.\t[BACKSPACE]" << endl;

    cout << "\n Press a key to proceed...";
    menuChoice = getche();

    return menuChoice;
}


int main()
{
    system("cls");

    MENU options;

    getch();
    return 0;
}
