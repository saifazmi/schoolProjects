#include <iostream.h>
#include <time.h> // for time countdown
#include <iomanip.h>
#include <stdlib.h> // for exit()
#include <conio.h>
#include <ctype.h>
#include <dos.h> // delay()

//structure for questions
struct Question
{
    const char* question;
    char ans;
};

//defining questions
Question Questions[] = {
    {
        "\tWhich is India\'s first Internet Service Provider (ISP) ?\n\n" //1
        "\t1. MTNL\t\t 2. Satyam Infoway\n"
        "\t3. VSNL\t\t 4. Mantra Online",
        '3' //store correct answer over here
    },
    {
        "\tWhich of these actors played the role in \'Kashmir ki Kali\' ?\n\n"//2
        "\t1. Joy Mukherjee\t\t 2. Shammi Kapoor\n"
        "\t3. Shashi Kapoor\t\t 4. Raj Kapoor",
        '2'
    },
    {
        "\tWhat is the duration of a normal pregnancy ?\n\n"//3
        "\t1. 280 days\t\t 2. 8 months\n"
        "\t3. 9 months\t\t 4. 290 days",
        '1'
    },
    {
        "\tWhere in India was the first Medical College started ?\n\n"//4
        "\t1. Varanasi\t\t 2. Delhi\n"
        "\t3. Calcutta\t\t 4. Chennai",
        '3'
    },
    {
        "\tThe Indian Prime Minister for Shortest time period was : \n\n"//5
        "\t1. Charan Singh\t\t 2. Lal Bahadur Shastri\n"
        "\t3. G.L.Nanda\t\t 4. None of these",
        '3'
    },
    {
        "\tWhich country, besides West Indies had won World Cup Cricket twice ?\n\n"//6
        "\t1. Australia\t\t 2. India\n"
        "\t3. England\t\t 4. Pakistan",
        '1'
    },
    {
        "\tWho owned the Peacock Throne ?\n\n"//7
        "\t1. Akbar\t\t 2. Jahangir\n"
        "\t3. Aurangazeb\t\t 4. Humayun",
        '2'
    },
    {
        "\tBesides Nathuram Vinayak Godse, who was the other person\n"
        "\tsentenced to death for Mahatama Gandhi\'s assasination ?\n\n"//8
        "\t1. Markandeya\t\t 2. Narendra Godse\n"
        "\t3. John Cherian\t\t 4. Narayan Apte",
        '4'
    },
    {
        "\tWho invented the Jet Engine ?\n\n"//9
        "\t1. James Wright\t\t 2. Frank Whittle\n"
        "\t3. Henri Ford\t\t 4. John Ludwig",
        '2'
    },
    {
        "\tWhich of these actors won the National Award twice ?\n\n"//10
        "\t1. All of these\t\t 2. Naseeruddin Shah\n"
        "\t3. Om Puri\t\t 4. Sanjeev Kumar",//prob with aling
        '1'
    },
    {
        "\tWhich city has the world's Largest Radiotelescope ?\n\n"//11
        "\t1. Paris\t\t 2. London\n"
        "\t3. New Mexico\t\t 4. Mumbai",//prob with aling
        '3'
    },
    {
        "\tWhom did Krishna wanted to kill in the battle of Kurukshetra ?\n\n"//12
        "\t1. Karna\t\t 2. Bheeshma\n"
        "\t3. Kirpacharya\t\t 4. Duryodhana",
        '2'
    },
    {
        "\tWhich famous studio once stood at the site where the\n"
        "\tFilm & Television Institute, Pune is located today ?\n\n"//13
        "\t1. Minerva\t\t 2. Filmistan\n"
        "\t3. Raj Kamal\t\t 4. Prabhat",
        '4'
    },
    {
        "\tWhich famous National Park in the USA has more than 4000 Hot Springs ?\n\n"//14
        "\t1. Greenwich National Park\t\t 2. Blue Stone National Park\n"
        "\t3. Yellow Stone National Park\t\t 4. Kanaha",
        '3'
    },
    {
        "\tHow many people attended Lord Buddha\'s first sermon & became his\n"
        "\toriginal disciples ?\n\n"//15
        "\t1. 8 people\t\t 2. 5 people\n"
        "\t3. 6 people\t\t 4. 7 people",
        '2'
    },
};
class main_Menu
{
    public:

    main_Menu()
    {
        clrscr();
        cout << "\a\n MADE BY:\n * Saifullah Azmi\n * Alka Rao\n * Ananjan Singh Rawat\n * Devpriya Singh\n * Vrinda Kapoor\n"<<endl;
        cout << "\n DELHI PUBLIC SCHOOL, Indira Nagar"<<endl;
        cout << "\n\n\n";
        cout <<"\t\t\t\tWelcome To GAMING ZONE"<<endl;
        cout << "\t\t\t\t_______________________" << endl;
    }

    int option()
    {
       int opt;
        clrscr();
        cout << "\n\n Please choose from one of these options-"<<endl;
        cout << "\n 1. Play QUIZ"<<endl;
        cout << "\n 2. Play HIGH n LOW"<<endl;
        cout << "\n 3. QUIT"<<endl;
        cout << "\n Please enter your choice (1-3): ";

        cin >> opt;

        return opt;
    }

};

class HnL
{
    public:

    HnL()
    {
        clrscr();
        cout <<"\t\t\t\tWelcome to HIGH n LOW"<<endl;
        cout << "\t\t\t\t____________________" << endl;
    }

    int option()
    {
        int opt;

        cout << "\n Please choose from one of these options-"<<endl;
        cout << "\n 1. Read the RULES"<<endl;
        cout << "\n 2. Play HIGH n LOW"<<endl;
        cout << "\n 3. Back to MAIN MENU"<<endl;
        cout << "\n Please enter your choice (1-3): ";

        cin >> opt;

        return opt;
    }
    char rules_hnl()
    {
        clrscr();
        char ch;
        cout << "\n\n RULES:"<<endl;
        cout << " --------"<<endl;
        cout << " This game is about guessing a no. between 0 to 100.\n You get 8 chances overall. All the best ! :-)";
        cout << "\n\n * Press \'E\' to Play HIGH n LOW."<<endl;
        cout << "\n * Press \'B\' to Go back to MAIN MENU."<<endl;
        ch = getche();

        return ch;
    }
};

class QUIZ
{
    public:

    QUIZ() //constructor
    {
        clrscr();
        cout <<"\t\t\t\tWelcome to THE QUIZ"<<endl;
        cout << "\t\t\t\t____________________" << endl;
    }

    int option()
    {
        int opt;

        cout << "\n Please choose from one of these options-"<<endl;
        cout << "\n 1. Read the RULES"<<endl;
        cout << "\n 2. Begin the QUIZ"<<endl;
        cout << "\n 3. Back to MAIN MENU"<<endl;
        cout << "\n Please enter your choice (1-3): ";

        cin >> opt;

        return opt;
    }

    char rules_quiz() //rules for the QUIZ
    {
        clrscr();
        char ch;
        int Num_Questions;
        Num_Questions = sizeof(Questions)/sizeof(*Questions);
        cout << "\n\n RULES:"<<endl;
        cout << " --------"<<endl;
        cout << "\n * This Quiz consists of "<< Num_Questions <<"  Questions."<<endl;
        cout << "\n * There is No Time Limit." << endl;
        cout << "\n * One WRONG answer and the Quiz ends. (So be careful)" << endl;
        cout << "\n * Scoring is done on the following bases - " << endl;
        cout << "\n\t--If you answer ALL the Questions Correctly:\tEXCELLENT"<<setw(20) << "***************"<< endl;
        cout << "\n\t--If you answer 10 to 14 Questions Correctly:\tGOOD" << setw(20)<<"**********"<< endl;
        cout << "\n\t--If you answer 5 to 9 Questions Correctly:\tAVERAGE" << setw(12) << "*****" << endl;
        cout << "\n\t--If you answer 1 to 4 Questions Correctly:\tPOOR"<<setw(11) << "*" << endl;
        cout << "\n\n * Press \'E\' to Enter THE QUIZ."<<endl;
        cout << "\n * Press \'B\' to Go back to MAIN MENU."<<endl;
        ch = getche();

        return ch;
    }

};

const int NumQuestions = sizeof(Questions)/sizeof(*Questions);
const float NumQues = sizeof(Questions)/sizeof(*Questions);

void pause_end();

/*way to mark question that have been answered
-- one way might be to use the ans field as a flag,
if the ans == 0 then the question has already been asked and answered.*/

int main()
{
    int choice_main, choice_games;
    char choice_rules;

    char name[60];

    int i = 0;
    float answeredCorrect = 0;
    int quesNum = 1;
    char uAns;

    float duration;
    clock_t start;

    int n,ch,uno,cno;
    int chances=0;


    main_Menu menu_obj; //constructor call

    cout << "\n Please Enter your NAME: ";
    cin.getline (name,60);


    do{
	choice_main = menu_obj.option();

        if (choice_main == 1)
        {
            QUIZ quiz_obj;
            choice_games = quiz_obj.option();
	    if (choice_games == 1)
            {
		choice_rules = quiz_obj.rules_quiz();
                {
		    if (choice_rules == 'E' || choice_rules == 'e')
			goto ques;
		}
	    }
	    else if (choice_games == 2)
	    {
		goto ques;
	    }
	}
        else if (choice_main == 2)
        {
            HnL hnl_obj;
            choice_games = hnl_obj.option();
	    if (choice_games == 1)
            {
		choice_rules = hnl_obj.rules_hnl();
                {
		    if (choice_rules == 'E' || choice_rules == 'e')
			goto random;
		}
            }
	    else if (choice_games == 2)
            {
                goto random;
            }
        }
        else if (choice_main == 3)
        {
            cout << "Bye Bye :)" << endl;
	    delay(2000);
            exit(0);
        }
    } while (choice_games == 3 || choice_rules == 'B' || choice_rules == 'b');

    random :

    clrscr();
    randomize();
    cno=random(100);

    cout<<"\t\t\t\tHIGH n LOW"<<endl;
    do{
        cout<<"\n\n Chance no."<<chances+1<<":";
        cin>>uno;
        chances++;

        if(uno==cno)
        {
            cout<<"\n Congratulations!You Won!! :D :D \n " ;
            getch();
            break;
        }
        else
        {
            if(uno<cno)
            {
                cout<<"\n Low \n";
            }
            else if(uno>cno)
            {
                cout<<"\n High \n";
            }
            if(chances==8)
            {
                cout<<"\n GAME OVER!!";
                getch();
            }
        }
    }while(chances<8);

    clrscr();
    cout<<"\n\n Player Name:\t" <<name;
    cout<<"\n\n No. of chances = "<<chances;
    delay(6000);
    pause_end();

    ques :

    clrscr();
    start = clock();
    while (answeredCorrect < NumQuestions)
        {
            if (Questions[i].ans != 0)
            {
                clrscr();
                cout << "\n Question "<<quesNum<<" : \n" << endl;
                quesNum++;
                delay(1000);
                cout << Questions[i].question << endl;//printing question using the variable from struct Questions
                delay(1000);
                cout << "\n\n Please Enter the CORRECT answer (1-4): ";
                cin >> uAns;
                if (Questions[i].ans == uAns)
                {
                    Questions[i].ans = 0; // now the question has been asked and asnwered
                    answeredCorrect++; //updating no. of answered ques for final result sheet
                    delay(1000);
                    cout << "\n Correct!!!" << endl;
                    delay(1000);
                }

                else
                {
                    delay(1000);
                    cout << "\a\n WRONG!!!" << endl;
                    delay(1000);
                    cout << "\n\t\t\t-----GAME OVER-----" << endl;
                    cout << "\n \"Better Luck Next Time\" :)" << endl;
                    delay(3000);
                    break;
                }
            }
            i = (i + 1) % NumQuestions;
        }

    duration = (clock() - start) / (double) CLOCKS_PER_SEC;

    clrscr();
    cout << "\n\n";
    if(answeredCorrect == 15)
    cout << "\tCongratulations "<<name<< " your performance was EXCELLENT."<<endl;
    else if(answeredCorrect <=14 && answeredCorrect >= 10)
    cout << "\tYou did well"<<name<< " your performance was GOOD." << endl;
    else if (answeredCorrect <=9 && answeredCorrect >=5)
    cout << "\tYour performance was AVERAGE " << name << endl;
    else if (answeredCorrect <=4)
    cout << "\tYou need to work hard "<<name<<" your performance was POOR." << endl;

    delay(1000);
    cout << "\n\tYou answered "<< answeredCorrect <<" question(s) correctly out of " << NumQuestions << endl;

    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout << setprecision(2);
    cout << "\n\tYour SCORE is "<< (answeredCorrect/NumQues)*100 <<"%"<< endl;
    duration /=60;
    delay(1000);
    cout << "\n\tAnd you took "<< duration << " Minutes to answer those questions"<<endl;
    delay(20000);
    pause_end();

    return 0;
}

void pause_end()
{
    clrscr();
    cout << "\n\n\n\n\n\n";
    cout << "\t\t\t\tThank You for playing..." << endl;
    cout << "\n\t\t\t\tSee you Next Time ^_^" << endl;
    cout << "\n\n\n\n Press any key to exit..." << endl;
    getch();
    exit(0);
}
