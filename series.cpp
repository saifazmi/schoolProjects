#include <iostream>
#include <math.h>

using namespace std;

void sum(int, int);

int main()
{
    int n=0,choice;

    cout << "Enter the number of terms: ";
    cin >> n;

    cout << "\nChoose the series - " << endl;
    cout << "1. Series." << endl;
    cout << "2. Series." << endl;
    cout << "3. Series." << endl;
    cout << "4. Series." << endl;
    cin >> choice;

    sum(n,choice);

    return 0;
}

void series_1(int ter)
{
    int i = 1, ans = 0;

    do
    {
        ans += i;
        ++i;

    } while(i<=(ter+1));

    cout << ans;
}

void series_2(int ter)
{

}

void sum(int terms, int ch)
{
    switch(ch)
    {
        case 1 : series_1(terms);
                    break;
        case 2 : series_2(terms);
                    break;
        case 3 : series_3(terms);
                    break;
        case 4 : series_4(terms);
                    break;
        default : "\a\n\nWarning - \n\nWrong Choice, please chose from 1 to 4";
                    break;
    }
}
